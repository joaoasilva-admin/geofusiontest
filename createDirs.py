import os

#Directories
paths = ['./data/rj/folds', './data/rj/normalizedFolds', './data/rj/scaledFolds', './data/sp', './finalPredictions', './metrics/class', './metrics/reg']

for path in paths:
	os.makedirs(path, exist_ok=True)

