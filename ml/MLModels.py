import numpy as np
from sklearn.svm import SVR
import matplotlib.pyplot as plt
from sklearn import linear_model
import pandas as pd
from sklearn import tree
from sklearn import svm
import sklearn.metrics as metrics
from sklearn.metrics import classification_report
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.grid_search import GridSearchCV
from sklearn import linear_model
from sklearn.tree import DecisionTreeRegressor
import time
import matplotlib.pyplot as plt
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import KMeans

class Regressors(object):

	def crossValidadion(self, prefixData, model, folderMetrics, dataType):
		dataTrain = None
		dataTest = None		
		pathFile = ""
		
		met = pd.DataFrame(columns=['fold0','fold1', 'fold2', 'fold3','fold4','fold5','fold6','fold7','fold8','fold9'], index=['meanAbsoluteError', 'meanSquaredError', 'medianAbsoluteError','r2'])
		#print(met)

		models =  {'M1': ['domiciliosA1',], 'M2': ['domiciliosA1', 'domiciliosA2'], 'M3': ['domiciliosA1', 'domiciliosA2', 'domiciliosB1'],'M4': ['domiciliosA1', 'domiciliosA2', 'domiciliosB1','rendaMedia'], 'M5': ['domiciliosA1', 'domiciliosA2', 'domiciliosB1','rendaMedia', 'popMaisDe60'], 'M6': ['popAte9', 'popDe10a14', 'popDe15a19', 'popDe20a24', 'popDe25a34', 'popDe35a49', 'popDe50a59', 'domiciliosA1', 'domiciliosA2', 'domiciliosB1', 'domiciliosC1', 'domiciliosC2', 'domiciliosD', 'domiciliosE', 'rendaMedia'], 'M7': ['popAte9', 'popDe10a14', 'popDe15a19', 'popDe20a24', 'popDe25a34', 'popDe35a49', 'popDe50a59', 'popMaisDe60', 'domiciliosA1', 'domiciliosA2', 'domiciliosB1', 'domiciliosB2', 'domiciliosC1', 'domiciliosC2', 'domiciliosD', 'domiciliosE', 'rendaMedia'] }


		for m, xNames in models.items():
			print(m)
			print(xNames)

			for fold in range(0, 10):
				print("--------- Start Fold ----------", fold)
				pathFile = prefixData+"training"+str(fold)+".csv"
				#print(pathFile)			
				dataTrain = pd.read_csv(pathFile, header = 0)
				#print(dataTrain)
				pathFile = prefixData+"test"+str(fold)+".csv"
				#print(pathFile)
				dataTest = pd.read_csv(pathFile, header = 0)
				#print(dataTest)
				met["fold"+str(fold)] = self.fitModel(dataTrain, dataTest, model, xNames, '')
				
				print("---------- End Fold -----------", fold)
				print()
			print(met)
			try:	
				print("")		
				met.to_csv(folderMetrics+model+m+dataType+".csv", sep=',')
			except Exception as e:
				raise e

	def fitModel(self,dataTrain, dataTest, model, xNames, pathFinalPrediction):
	#	xNames = ['popAte9', 'popDe10a14', 'popDe15a19', 'popDe20a24', 'popDe25a34', 'popDe35a49', 'popDe50a59', 'popMaisDe60', 'domiciliosA1', 'domiciliosA2', 'domiciliosB1', 'domiciliosB2', 'domiciliosC1', 'domiciliosC2', 'domiciliosD', 'domiciliosE', 'rendaMedia']
		# domA1, domA2, domB1, rendaMedia


		X = dataTrain.ix[:, xNames]
		Y = dataTrain.ix[:,'faturamento']
		reg = None
		if model == "linear":
			reg = linear_model.LinearRegression().fit(X,Y)
		elif model == 'rbf':
			c, gama = self.parametersSearchRBF(X,Y)
			reg = SVR(kernel='rbf', C=c, gamma=gama).fit(X,Y)		
		elif model == 'dt':
			md = self.parametersSearchDT(X,Y)
			reg =  DecisionTreeRegressor(max_depth = md).fit(X,Y)
 	
		predict = reg.predict(dataTest.ix[:, xNames])
		#print(predict)

		'''lw = 2
		plt.scatter(dataTest.ix[:, xNames], dataTest.ix[:,'faturamento'], color='darkorange', label='data')
		plt.hold('on')
		plt.plot(dataTest.ix[:, xNames], predict, color='navy', lw=lw, label='Linear')
		plt.xlabel('data')
		plt.ylabel('target')
		plt.title('Support Vector Regression')
		plt.legend()
		plt.show()'''

		if pathFinalPrediction != '':
			final = pd.DataFrame(columns=['codigo', 'nome', 'faturamento'])
			final['codigo'] = dataTest['codigo']
			final['nome'] = dataTest['nome']
			final['faturamento'] = predict
			try:	
				print("")		
				final.to_csv(pathFinalPrediction, sep=',',index=False)
			except Exception as e:
				raise e
		else:		
			met = np.array([metrics.mean_absolute_error(dataTest.ix[:,'faturamento'], predict), metrics.mean_squared_error(dataTest.ix[:,'faturamento'], predict), metrics.median_absolute_error(dataTest.ix[:,'faturamento'], predict), metrics.r2_score(dataTest.ix[:,'faturamento'], predict)])	
			print(met)
			return met

		

	def parametersSearchRBF(self, X, Y):
		parameter_candidates = [ {'C': list(range(1,101,10)), 'gamma': np.arange(0.1, 1.0, 0.1), 'kernel': ['rbf']},]

		reg = GridSearchCV(estimator=svm.SVR(), param_grid=parameter_candidates, n_jobs=-1, cv=10)
		reg.fit(X, Y)   

		print('Best score for data1:', reg.best_score_)
		print('Best C:',reg.best_estimator_.C)		
		print('Best Gamma:',reg.best_estimator_.gamma)
		time.sleep(7)
		return reg.best_estimator_.C, reg.best_estimator_.gamma

	
	def parametersSearchDT(self, X, Y):
		parameter_candidates = [ {'max_depth': list(range(2,25,1))},]

		reg = GridSearchCV(estimator=DecisionTreeRegressor(), param_grid=parameter_candidates, n_jobs=-1, cv=10)
		reg.fit(X, Y)   

		print('Best score for data1:', reg.best_score_)
		print('Best max Depth:',reg.best_estimator_.max_depth )						
		return reg.best_estimator_.max_depth 


class Classifiers(object):

	def crossValidadion(self, prefixData, model, folderMetrics, dataType):
		dataTrain = None
		dataTest = None		
		pathFile = ""
		
		met = pd.DataFrame(columns=['fold0','fold1', 'fold2', 'fold3','fold4','fold5','fold6','fold7','fold8','fold9'], index=['accuracy', 'microF1','macroF1'])

		#print(met)
		for fold in range(0, 10):
			print("--------- Start Fold ----------", fold)
			pathFile = prefixData+"training"+str(fold)+".csv"
			#print(pathFile)			
			dataTrain = pd.read_csv(pathFile, header = 0)
			#print(dataTrain)
			pathFile = prefixData+"test"+str(fold)+".csv"
			#print(pathFile)
			dataTest = pd.read_csv(pathFile, header = 0)
			#print(dataTest)
			met["fold"+str(fold)] = self.adjusteModel(dataTrain, dataTest, model, '')
			
			print("---------- End Fold -----------", fold)
			print()
		print(met)
		try:	
			print("")		
			met.to_csv(folderMetrics+model+dataType+".csv", sep=',')
		except Exception as e:
			raise e

	def parametersSearchSVM(self, X, Y):
		parameter_candidates = [ {'C': list(range(1,101,10)), 'gamma': np.arange(0.1, 1.0, 0.1), 'kernel': ['rbf']},]

		clf = GridSearchCV(estimator=svm.SVC(), param_grid=parameter_candidates, n_jobs=-1, cv=10)
		clf.fit(X, Y)   

		print('Best score for data1:', clf.best_score_)
		print('Best C:',clf.best_estimator_.C)		
		print('Best Gamma:',clf.best_estimator_.gamma)
		time.sleep(2)
		return clf.best_estimator_.C, clf.best_estimator_.gamma

	def parametersSearchDecisionTree(self, X, Y):		
		parameter_candidates = [{'max_depth': np.arange(1, 10)},{'criterion':['gini', 'entropy']}]

		clf = GridSearchCV(tree.DecisionTreeClassifier(), parameter_candidates)
		clf.fit(X, Y)		
		
		print('Best depth:',clf.best_estimator_.max_depth)		
		print('Best criterion:',clf.best_estimator_.criterion)
		
		return clf.best_estimator_.max_depth, clf.best_estimator_.criterion
	
	def parametersSearchRandomForest(self, X, Y):		
		parameter_candidates = [{'n_estimators': np.arange(1, 50)},{'criterion':['gini', 'entropy']}]
		clf = GridSearchCV(RandomForestClassifier(), parameter_candidates)
		clf.fit(X, Y)		
				
		print('Best criterion:',clf.best_estimator_.criterion)
		print('Best nEstimators:',clf.best_estimator_.n_estimators)
		time.sleep(2)
		return clf.best_estimator_.criterion, clf.best_estimator_.n_estimators


	def adjusteModel(self, dataTrain, dataTest, model, pathFinalPrediction):
		xNames = ['popAte9', 'popDe10a14', 'popDe15a19', 'popDe20a24', 'popDe25a34', 'popDe35a49', 'popDe50a59', 'popMaisDe60', 'domiciliosA1', 'domiciliosA2', 'domiciliosB1', 'domiciliosB2', 'domiciliosC1', 'domiciliosC2', 'domiciliosD', 'domiciliosE', 'rendaMedia']

		X = dataTrain.ix[:, xNames]
		Y = dataTrain.ix[:,'potencial']

	
		#print(Y)
		predict = None
		clf = None
		if model == "dt":
			depht, crit= self.parametersSearchDecisionTree(X, Y)
			clf = tree.DecisionTreeClassifier(max_depth=depht, criterion=crit).fit(X, Y)			
		elif model == "svm":
			c, gama = self.parametersSearchSVM(X, Y)
			clf = svm.SVC(C=c, kernel='rbf', gamma=gama).fit(X, Y) 
			#			svc = svm.SVC(kernel='linear', C=C).fit(X, y)
			#rbf_svc = svm.SVC(kernel='rbf', gamma=0.7, C=C).fit(X, y)
			#poly_svc = svm.SVC(kernel='poly', degree=3, C=C).fit(X, y)
			#lin_svc = svm.LinearSVC(C=C).fit(X, y)
		elif model == "nb":			
			clf = GaussianNB().fit(X, Y)
		else:
			crit, n_est= self.parametersSearchRandomForest(X,Y)
			clf = RandomForestClassifier(criterion=crit, n_estimators = n_est).fit(X,Y)
	
		predict = clf.predict(dataTest.ix[:, xNames])

		if pathFinalPrediction != '':
			final = pd.DataFrame(columns=['codigo', 'nome', 'potencial'])
			final['codigo'] = dataTest['codigo']
			final['nome'] = dataTest['nome']
			final['potencial'] = predict
			try:	
				print("")		
				final.to_csv(pathFinalPrediction, sep=',',index=False)
			except Exception as e:
				raise e

		else:
			met = np.array([metrics.accuracy_score(dataTest.ix[:,'potencial'], predict),metrics.f1_score(dataTest.ix[:,'potencial'], predict, average='micro') ,metrics.f1_score(dataTest.ix[:,'potencial'], predict, average='macro')])	
			print(met)
			return met
			
