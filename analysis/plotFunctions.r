# function for plot Regression Models
plotRegression<- function(m1,m2,m3,m4,m5,m6,m7, tit){
  
  mErrors <- c(rowMeans(m1[1,2:11]), rowMeans(m2[1,2:11]), rowMeans(m3[1,2:11]), 
               rowMeans(m4[1,2:11]), rowMeans(m5[1,2:11]), rowMeans(m6[1,2:11]),
               rowMeans(m7[1,2:11]))
  
  dat <- data.frame(model = c("M1","M2", "M3","M4", "M5", "M6", "M7"), 
                    meansErrors = mErrors)
  g <- ggplot(dat , aes(x=model, y=meansErrors, fill =meansErrors))
  g +  geom_bar(colour="black", stat="identity") +  guides(fill=FALSE) + 
    ggtitle(tit)+geom_text(aes(label=round(meansErrors, digits = 2)), vjust=1.5)
}

# function for plot Classification Measures
plotClasifaction <- function(mNormal, mScaled, mNormalized, tit){
  mNormal$Mean <- rowMeans(mNormal[,2:11])
  mScaled$Mean <- rowMeans(mScaled[,2:11])
  mNormalized$Mean <- rowMeans(mNormalized[,2:11])
  
  colnames(mNormal)[1] <- 'Measure'
  colnames(mScaled)[1] <- 'Measure'
  colnames(mNormalized)[1] <- 'Measure'
  # plots
  p1 <- ggplot(mNormal, aes(y=Mean, x=Measure, fill=Measure)) + 
    geom_bar(stat="identity") + coord_cartesian(ylim = c(0, 1))+  
    theme(legend.position="none", axis.text.x=element_blank(), 
          plot.title = element_text(size = 12) ) + ggtitle("Normal Data") +  
    geom_text(aes(label=round(Mean, digits = 2)), vjust=1.5)
  
  
  p2 <- ggplot(mScaled, aes(y=Mean, x=Measure, fill=Measure)) + 
    geom_bar(position="dodge", stat="identity") + coord_cartesian(ylim = c(0, 1))+   
    theme(legend.position="none", axis.text.x = element_blank(), 
          plot.title = element_text(size = 12))+   ggtitle("Scaled Data") +  
    geom_text(aes(label=round(Mean, digits = 2)), vjust=1.5)
  
  p3 <- ggplot(mNormalized, aes(y=Mean, x=Measure, fill=Measure)) + 
    geom_bar(position="dodge", stat="identity") + coord_cartesian(ylim = c(0, 1))+     
    theme(legend.position="none", axis.text.x=element_blank(), 
          plot.title = element_text(size = 12))+ ggtitle("Normalized Data")+    
    geom_text(aes(label=round(Mean, digits = 2)), vjust=1.5)
  
  legend <- get_legend(p1 + theme(legend.position="bottom"))
  title <- ggdraw() + draw_label(tit, fontface='bold')
  prow <- plot_grid(p1 , p2 ,p3, nrow = 1)
  
  p<-plot_grid(prow, legend, ncol = 1, rel_heights = c(1, .2))
  plot_grid(title, p, ncol=1, rel_heights=c(0.1, 1))
}


# function for pie plot
piePlot <- function(data){
  slices <- sums$x
  lbls <- sums$segmentos
  pct <- round(slices/sum(slices)*100)
  #lbls <- paste(lbls, pct) # add percents to labels 
  #lbls <- paste(lbls,"%",sep="") # ad % to labels 
  pie(slices,labels = lbls, col=rainbow(length(lbls)),
      main="Segmentos")
  #legend("topright", list(segmentos=x$segmentos) ,fill = sums$segmentos)
}
