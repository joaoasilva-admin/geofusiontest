import os

#necessary to create the pdfs
os.putenv("RSTUDIO_PANDOC", "/usr/lib/rstudio/bin/pandoc")

#create directories from files
os.system("python3 createDirs.py")  

#pre processing data
os.system("python3 mainPreprocesing.py")  

#make data exploratory analysis
os.system('''R -e "rmarkdown::render('exploratoryDataAnalysis/Analysis.Rmd')"''')  

#fit machine learning models
os.system("python3 mainML.py")  
#make data exploratory analysis
os.system('''R -e "rmarkdown::render('analysis/Report.Rmd')"''')  






