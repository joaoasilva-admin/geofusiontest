import unittest
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from preProcessing import PreProcessing


class TestFiles(unittest.TestCase):
	global pp
	
	def test_loadRJ(self):   
		pp = PreProcessing() 	
		self.assertIsNotNone(pp.loadData("../data/rj.csv"))

	def test_loadSP(self):   
		pp = PreProcessing() 	
		self.assertIsNotNone(pp.loadData("../data/sp.csv"))

	def test_dropAllZeroesRows(self):
		pp = PreProcessing()
		df = pd.DataFrame({'a':[0,0,1,1], 'b':[0,1,0,1]})
		self.assertEqual(str(pp.removeRowsAllZeroes(df)),"   a  b\n1  0  1\n2  1  0\n3  1  1")

	def test_SplitKFolds(self):
		pp = PreProcessing()
		
		self.assertEqual(pp.splitKFolds(pp.loadData("../data/rj.csv"),10, "../data/folds/"),"")
		

	def test_scaleData(self):
		pp = PreProcessing()
		X = [[ 1., -1.,  2.], [2.,  0.,  0.], [ 0.,  1., -1.]]
		Y = [[-1.,  1., 0.]]
		self.assertEqual(pp.scaleData(X, Y).all() , np.array([[-2.44948974,  1.22474487, -0.26726124]]).all())

	def test_normalizeData(self):
		pp = PreProcessing()
		X = [[ 1., -1.,  2.], [2.,  0.,  0.], [ 0.,  1., -1.]]
		Y = [[-1.,  1., 0.]]
		self.assertEqual(pp.normalizeData(X, Y).all() , np.array([[-0.70710678,  0.70710678,  0.        ]]).all())
   
	def test_createRegressionFiles(self):
		pp = PreProcessing()		
		df1 = pp.createRegressionFiles(pp.loadData("rj.csv"), "rjR.csv")
		df2 = pp.loadData("rjReg.csv")
		self.assertEqual(df1.equals(df2), True)

	def test_createClassificationFiles(self):
		pp = PreProcessing()		
		df1 = pp.createClassificationFiles(pp.loadData("rj.csv"), "rjS.csv")
		df2 = pp.loadData("rjClas.csv")
		self.assertEqual(df1.equals(df2), True)
    

if __name__ == '__main__':
    unittest.main()