import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.cross_validation import StratifiedKFold
from sklearn import preprocessing


class PreProcessing(object):
	"""Class for  data pre processing"""
	
	def loadData(self, pathIn):
		data = None
		try:
			data = pd.read_csv(pathIn)			
		except Exception as e:
			raise e

		return data


	def removeRowsAllZeroes(self, dataFrame):
		return  dataFrame[(dataFrame.T != 0).any()]


	def normalizeData(self, dataFrame1, dataFrame2):
		normalizer = preprocessing.Normalizer().fit(dataFrame1)
		return normalizer.transform(dataFrame2) 

	def scaleData(self, dataFrame1, dataFrame2):
		scaler = preprocessing.StandardScaler().fit(dataFrame1)
		return scaler.transform(dataFrame2)  

	def splitKFolds(self, dataFrame, kFolds, folderOut):
		
		X=dataFrame[dataFrame.columns.difference(['potencial'])]		
		y = dataFrame['potencial']		

		skf = StratifiedKFold(y, n_folds=kFolds)
		i = 0
		for train_index, test_index in skf:	
			try:		
				df = dataFrame.ix[train_index]			
				df.to_csv(folderOut+"training"+str(i)+".csv", sep=',')
			except Exception as e:
				raise e
			
			try:
				df = dataFrame.ix[test_index]			
				df.to_csv(folderOut+"test"+str(i)+".csv", sep=',')
			except Exception as e:
				raise e
			i = i+1





	def createNormalizedFiles(self, dataFrame1, dataFrame2, pathOut1, pathOut2):
		X1 = dataFrame1[dataFrame1.columns.difference(['codigo',	'nome',	'cidade', 'estado',	'população','potencial','faturamento'])]		
		X2 = dataFrame2[dataFrame2.columns.difference(['codigo',	'nome',	'cidade', 'estado',	'população','potencial','faturamento'])]	
		
		normalizer = preprocessing.Normalizer().fit(X1)				

		newDF1 = pd.concat([dataFrame1[dataFrame1.columns[0:5]], pd.DataFrame(normalizer.transform(X1), columns=dataFrame1.columns[5:22]) , dataFrame1[dataFrame1.columns[22:]]], axis=1)

		newDF2 = pd.concat([dataFrame2[dataFrame2.columns[0:5]], pd.DataFrame(normalizer.transform(X2), columns=dataFrame2.columns[5:22]) , dataFrame2[dataFrame2.columns[22:]]], axis=1)

		try:		
			newDF1.to_csv(pathOut1, sep=',')
		except Exception as e:
			raise e

		try:		
			newDF2.to_csv(pathOut2, sep=',')
		except Exception as e:
			raise e
		
		

	def createScaledFiles(self, dataFrame1, dataFrame2, pathOut1, pathOut2):
		X1 = dataFrame1[dataFrame1.columns.difference(['codigo',	'nome',	'cidade', 'estado',	'população','potencial','faturamento'])]		
		X2 = dataFrame2[dataFrame2.columns.difference(['codigo',	'nome',	'cidade', 'estado',	'população','potencial','faturamento'])]	
		
		scaler = preprocessing.StandardScaler().fit(X1)

		newDF1 = pd.concat([dataFrame1[dataFrame1.columns[0:5]], pd.DataFrame(scaler.transform(X1), columns=dataFrame1.columns[5:22]) , dataFrame1[dataFrame1.columns[22:]]], axis=1)

		newDF2 = pd.concat([dataFrame2[dataFrame2.columns[0:5]], pd.DataFrame(scaler.transform(X2), columns=dataFrame2.columns[5:22]) , dataFrame2[dataFrame2.columns[22:]]], axis=1)

		try:		
			newDF1.to_csv(pathOut1, sep=',')
		except Exception as e:
			raise e

		try:		
			newDF2.to_csv(pathOut2, sep=',')
		except Exception as e:
			raise e
		



