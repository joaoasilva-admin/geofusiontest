import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import time

from ml.MLModels import Regressors
from ml.MLModels import Classifiers

def main():
	# print command line arguments
	for arg in sys.argv[0:]:
		print (arg)
	
	reg = Regressors()
	reg.crossValidadion("data/rj/folds/", "linear", "metrics/reg/", "Normal")
	'''	time.sleep(1)
	reg.crossValidadion("data/rj/scaledFolds/", "linear", "metrics/reg/", "Scaled")
	time.sleep(1)
	reg.crossValidadion("data/rj/normalizedFolds/", "linear", "metrics/reg/", "Normalized")
	time.sleep(1)'''

	reg.crossValidadion("data/rj/folds/", "dt", "metrics/reg/", "Normal")
	'''	time.sleep(1)
	reg.crossValidadion("data/rj/scaledFolds/", "dt", "metrics/reg/", "Scaled")
	time.sleep(1)
	reg.crossValidadion("data/rj/normalizedFolds/", "dt", "metrics/reg/", "Normalized")
	time.sleep(1)'''
	

	#reg.crossValidadion("data/rj/folds/", "rbf", "metrics/reg/", "Normal")	
	time.sleep(3)	
	reg.crossValidadion("data/rj/scaledFolds/", "rbf", "metrics/reg/", "Scaled")
	'''time.sleep(3)
	reg.crossValidadion("data/rj/normalizedFolds/", "rbf", "metrics/reg/", "Normalized")
	time.sleep(3)'''

	#final Prediction	
	dataTrain = pd.read_csv('data/rj.csv', header = 0)
	dataTest = pd.read_csv('data/sp.csv', header = 0)	
	reg.fitModel(dataTrain, dataTest, 'dt', ['popAte9', 'popDe10a14', 'popDe15a19', 'popDe20a24', 'popDe25a34', 'popDe35a49', 'popDe50a59', 'domiciliosA1', 'domiciliosA2', 'domiciliosB1', 'domiciliosC1', 'domiciliosC2', 'domiciliosD', 'domiciliosE', 'rendaMedia'], "finalPredictions/spFaturamentoFinalPrediction.csv")
 
	clas = Classifiers()
	
	#dt
	clas.crossValidadion("data/rj/folds/", "dt", "metrics/class/", "Original")	
	clas.crossValidadion("data/rj/scaledFolds/", "dt", "metrics/class/", "Scaled")	
	clas.crossValidadion("data/rj/normalizedFolds/", "dt", "metrics/class/", "Normalized")
	
	#rf
	clas.crossValidadion("data/rj/folds/", "rf", "metrics/class/", "Original")
	time.sleep(1)
	clas.crossValidadion("data/rj/scaledFolds/", "rf", "metrics/class/", "Scaled")
	time.sleep(1)
	clas.crossValidadion("data/rj/normalizedFolds/", "rf", "metrics/class/", "Normalized")
	time.sleep(1)

	#nb
	clas.crossValidadion("data/rj/folds/", "nb", "metrics/class/", "Original")	
	clas.crossValidadion("data/rj/scaledFolds/", "nb", "metrics/class/", "Scaled")	
	clas.crossValidadion("data/rj/normalizedFolds/", "nb", "metrics/class/", "Normalized")

	#svm
	clas.crossValidadion("data/rj/folds/", "svm", "metrics/class/", "Original")
	time.sleep(1)
	clas.crossValidadion("data/rj/scaledFolds/", "svm", "metrics/class/", "Scaled")
	time.sleep(1)
	clas.crossValidadion("data/rj/normalizedFolds/", "svm", "metrics/class/", "Normalized")
	time.sleep(1)

	
	dataTrain = pd.read_csv('data/rj.csv', header = 0)
	dataTest = pd.read_csv('data/sp.csv', header = 0)	
	clas.adjusteModel(dataTrain, dataTest, 'rf', "finalPredictions/spPotencialFinalPrediction.csv")
	
	



if __name__ == "__main__":
    main()