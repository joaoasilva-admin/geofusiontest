import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from preProcessing.preProcessing import PreProcessing

def main():
	# print command line arguments
	for arg in sys.argv[0:]:
		print (arg)
	
	pp = PreProcessing()

	#Load rj and sp data
	rjdf = pp.loadData("data/rj.csv")
	spdf = pp.loadData("data/sp.csv")

	#Create normalize files
	pp.createNormalizedFiles(rjdf,spdf,"data/rj/rjNormalized.csv", "data/sp/spNormalized.csv")
	#Create scaled files
	pp.createScaledFiles(rjdf,spdf,"data/rj/rjScaled.csv", "data/sp/spScaled.csv")

	 #create folds for crossvalidadtion
	pp.splitKFolds(pp.loadData("data/rj.csv"), 10, "data/rj/folds/")	
	pp.splitKFolds(pp.loadData("data/rj/rjNormalized.csv"),10, "data/rj/normalizedFolds/")
	pp.splitKFolds(pp.loadData("data/rj/rjScaled.csv"),10, "data/rj/scaledFolds/")
	



'''
	#create regression files for rj
	pp.createRegressionFiles(rjdf, "data/rj/rjReg.csv")
	pp.createRegressionFiles(pp.loadData("data/rj/rjNormalized.csv"),"data/rj/rjNormalizedReg.csv")	
	pp.createRegressionFiles(pp.loadData("data/rj/rjScaled.csv"),"data/rj/rjScaledReg.csv")

	#create classification files for rj
	pp.createClassificationFiles(rjdf, "data/rj/rjClass.csv")
	pp.createClassificationFiles(pp.loadData("data/rj/rjNormalized.csv"),"data/rj/rjNormalizedClass.csv")	
	pp.createClassificationFiles(pp.loadData("data/rj/rjScaled.csv"),"data/rj/rjScaledClass.csv")	
	
	#create regression files for sp
	pp.createRegressionFiles(spdf, "data/sp/spReg.csv")
	pp.createRegressionFiles(pp.loadData("data/sp/spNormalized.csv"),"data/sp/spNormalizedReg.csv")	
	pp.createRegressionFiles(pp.loadData("data/sp/spScaled.csv"),"data/sp/spScaledReg.csv")

	#create classification files for rj
	pp.createClassificationFiles(spdf, "data/sp/spClass.csv")
	pp.createClassificationFiles(pp.loadData("data/sp/spNormalized.csv"),"data/sp/spNormalizedClass.csv")	
	pp.createClassificationFiles(pp.loadData("data/sp/spScaled.csv"),"data/sp/spScaledClass.csv")
'''
   


if __name__ == "__main__":
    main()